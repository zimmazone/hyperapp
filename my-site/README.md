# What is this?

Hyperapp v2 boilerplate, using Parcel as the bundler/build server since it does not require configuration. This allows you to focus on hyperapp, not the tooling.

# Recommended Pre-requisites

- Node.js version 10+
- Ensure no other processes are running on ports 8080 or 8081

# Cloud 9 Instructions

1. In Codesandbox.io, click "File -> Export to ZIP"
2. Open C9.io workspace and create a folder for this project.
3. On C9, click "File -> Upload Local Files", and copy the boilerplate stuff.
4. In the C9 terminal, `cd` into the project folder.
5. `npm install` to download the dependencies.
6. `npm run c9` to start the build server.
7. Click "Preview Application", and try eding some JS or CSS to see if the "hot reloading" is working.

I tested this once, so I am fairly confident it will work.
