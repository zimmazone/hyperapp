import { h } from 'hyperapp'


//  const navCss1 = 'hidden md:flex flex-row'
//  const aCss1 = 'flex flex-row ml-2 mr-4 no-underline text-black'

/*
  For demonstration purposes, our application is only
  concerned with toggling a nav menu open/closed.
*/
export const initialState = {
  navOpen: false,
}

/*
  This function is a hyperapp Action.
  When this is attached to a user event, such as a button click,
  hyperapp will call this function, passing in the current app state.
  Actions _must_ return a new copy of the application state to trigger
  a re-render.
*/

 const navCss2 = 'md:hidden w-48 -ml-16 pl-20 pt-4 absolute h-screen border-r border-grey list-reset text-lg border-r-shadow bg-white border-grey'
 const aCss2 = 'mb-2 flex flex-col no-underline text-black'


export function NavMenuSm() {
  return (
    <nav class={navCss2}>
      <a href="#" class={aCss2}>About</a>
      <a href="#" class={aCss2}>Projects</a>
      <a href="#" class={aCss2}>Blog</a>
    </nav>
  )
}


function NavMenuLg() {
  return (
    <section class="flex flex-wrap">
        <div class="block hidden md:flex flex-row ">
          <a href="#" class="mr-2 no-underline text-black">About</a>
          <a href="#" class="mr-2 no-underline text-black">Projects</a>
          <a href="#" class="no-underline text-black">Blog</a>
        </div>
        <div class="flex flex-grow ml-20">
          <a class="ml-3" href="https://twitter.com/adamumdankore" target="_blank" rel="noopener"><i data-feather="twitter"></i></a>
          <a class="ml-3" href="https://www.linkedin.com/in/adamumuhammad" target="_blank" rel="noopener"><i data-feather="linkedin"></i></a>
          <a class="ml-3" href="mailto:adamu.dankore@gmail.com"><i data-feather="mail"></i></a>
          <a class="ml-3" href="tel:+1-319-296-5799"><i data-feather="phone"></i></a>
        </div>
    </section>
  )
}

function ToggleNav(state) {
  return {
    // Return new object
    ...state, // Copy existing state
    navOpen: !state.navOpen, // Modify the property this action cares about
  }
}

export function Header() {
    return (
    <header class="shadow-lg pt-6 pb-4">
      <div class="max-w-md m-auto pl-4 flex items-center">
         <button onclick={ToggleNav} class="flex ml-px md:hidden">
           <svg class="fill-current h-6 w-6" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <title>Menu</title>
           <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
           </svg>
          </button>
          <NavMenuLg />
        </div>
    </header>
        )
}