import { h } from 'hyperapp'

export function About() {
    return (
        <div class="p-4 mt-6 shadow-md rounded">
          I have a bachelor of arts degree in <mark>Chemistry</mark> and a minor in <mark>Leadership</mark>.
        </div>
        )
}