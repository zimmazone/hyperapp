import { h } from 'hyperapp'

export function Intro() {
    return (
        <div class="pin-y p-4 shadow-md rounded">
         <p class="mt-6">Thank you for stopping by!</p>
            <p class="mb-6 mt-4">
                My name is Adamu and I am building <a href="https:/homeawayfromhome.online" target="_blank" rel="noopener"><mark>HAFH</mark></a> with Mike Montoya since 2015.
            </p>
            <p class="mb-6">
                 I am actively searching for opportunities in the software field in order to switch my career from a <mark>Chemist</mark> in the pharmaceutical industry to a <mark>Softwarre Developer.</mark>
            </p>
            <p class="mb-6">
               My biggest project to date is @HAFH, an online platform for people living abroad and natives who celebrate their ancestry.
            </p>
        </div>
        )
}