/*
  Basic hyperapp v2 boilerplate.
  WIP docs for HA v2: https://github.com/jorgebucaran/hyperapp/blob/V2/README.md
*/
import "./styles.css"
import { Header, initialState, NavMenuSm } from "./components/Navigation.js"
import { h, app } from "hyperapp"
import { Intro } from "./components/Intro.js"
import { About } from "./components/About.js"


/*
  Root app view
  Hyperapp passes in the application state
  You need to return a virtual-dom tree by JSX or 'h' function.
  This root view cannot return an array.
*/
function AppView(state) {
  return (
    <div class="">
      <Header />
      <main class="max-w-md mx-auto text-2xl leading-normal relative">
      {state.navOpen && <NavMenuSm />}
        <Intro />
        <About />
      </main>
    </div>
  )
}

/*
  Start hyperapp (with fewest concepts)
  - init: Initial application state
  - view: function which turns application state into hyperapp's virtual-DOM
  - container: HTML element which hyperapp will mount into
*/
app({
  init: initialState,
  view: AppView,
  container: document.getElementById("app"),
})
